#ifndef LIBRARY_H
#define LIBRARY_H

#include "pdfdocument.h"

#include <QString>

namespace QtPdfium {

class Library {
public:
    Library();
    ~Library();

public:
    enum Error{
        None
    };

public:
    Document* open(const QString& path);
    Error lastError() const;

private:
    struct Impl;
    Impl* _impl;
};
}

#endif // LIBRARY_H
