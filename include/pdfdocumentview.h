#ifndef PDFDOCUMENTVIEW_H
#define PDFDOCUMENTVIEW_H

#include <QWidget>
#include "pdflibrary.h"

namespace Ui {
class PdfDocumentView;
}

class PdfDocumentView : public QWidget
{
    Q_OBJECT

public:
    explicit PdfDocumentView(QWidget *parent = 0);
    ~PdfDocumentView();

public:
    void setDocument(QtPdfium::Document* document);
    QtPdfium::Document* document() const;

    void setPage(int pageNumber);
    QtPdfium::Page* page() const;

private:
    void _renderCurrentPage();

private:
    Ui::PdfDocumentView *ui;

    QtPdfium::Document* _document;
    QtPdfium::Page* _currentPage;
};

#endif // PDFDOCUMENTVIEW_H
