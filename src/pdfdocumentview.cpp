#include "pdfdocumentview.h"
#include "ui_pdfdocumentview.h"

#include <QGraphicsPixmapItem>

PdfDocumentView::PdfDocumentView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PdfDocumentView)
{
    ui->setupUi(this);

    QGraphicsScene* scene = new QGraphicsScene();
    ui->PdfView->setScene(scene);

    _document = nullptr;
    _currentPage = nullptr;
}

PdfDocumentView::~PdfDocumentView()
{
    delete ui;
}

void PdfDocumentView::setDocument(QtPdfium::Document* document)
{
    _document = document;
    setPage(0);
}

QtPdfium::Document* PdfDocumentView::document() const
{
    return _document;
}

void PdfDocumentView::setPage(int pageNumber)
{
    _currentPage = _document->getPage(pageNumber);

    if (_currentPage) {
        _renderCurrentPage();
    }
}

void PdfDocumentView::_renderCurrentPage()
{
    if (!_currentPage) {
        return;
    }

    QSize pageSize = _currentPage->getSize();
    QSize canvasSize = ui->PdfView->size();

    float scaleFactor = (float)canvasSize.width() / (float)pageSize.width();
    QSize scaledSize(canvasSize.width(), int(pageSize.height() * scaleFactor));

    QImage image = _currentPage->render(scaledSize);

    QGraphicsScene* scene = ui->PdfView->scene();
    QGraphicsPixmapItem* item = new QGraphicsPixmapItem(QPixmap::fromImage(image));
    scene->addItem(item);
}

QtPdfium::Page* PdfDocumentView::page() const
{
    return _currentPage;
}
