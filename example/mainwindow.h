#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "qtpdfium.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionOpen_triggered();

    void on_actionPrint_triggered();

    void on_actionExit_triggered();

private:
    Ui::MainWindow *ui;

    QtPdfium::Library* _pdfLibrary;
    QtPdfium::Document* _currentDocument;
};

#endif // MAINWINDOW_H
