#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QStringList>
#include <QPrinter>
#include <QPrintDialog>
#include <QPainter>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _pdfLibrary = new QtPdfium::Library();
    _currentDocument = nullptr;

    showMaximized();
}

MainWindow::~MainWindow()
{
    delete ui;

    if (_currentDocument) {
        delete _currentDocument;
    }

    delete _pdfLibrary;
}

void MainWindow::on_actionOpen_triggered()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setNameFilter(tr("PDF Documents (*.pdf)"));

    QStringList fileNames;
    if (dialog.exec()){
        fileNames = dialog.selectedFiles();

        _currentDocument = _pdfLibrary->open(fileNames[0]);

        if (_currentDocument) {
            ui->PdfView->setDocument(_currentDocument);
        }
    }
}

void MainWindow::on_actionPrint_triggered()
{
    if (_currentDocument){
        QPrinter printer;
        QPrintDialog* dialog = new QPrintDialog(&printer,0);

        if(dialog->exec() == QDialog::Accepted) {
            QtPdfium::Page* page = _currentDocument->getPage(0);
            QSize pageSize = page->getSize();
            QImage image = page->render(pageSize);

            QPainter painter(&printer);
            painter.drawImage(QPoint(0,0),image);
            painter.end();
        }

        dialog->deleteLater();
    }
}

void MainWindow::on_actionExit_triggered()
{
    QApplication::quit();
}
