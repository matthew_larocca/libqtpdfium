#-------------------------------------------------
#
# Project created by QtCreator 2018-03-09T23:40:23
#
#-------------------------------------------------

QT       += core gui widgets printsupport

INCLUDEPATH += include

TARGET = example
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        example/main.cpp \
        example/mainwindow.cpp

HEADERS += \
        example/mainwindow.h

FORMS += \
        example/mainwindow.ui

win32:debug {
    LIBS += "$$PWD/pdfium/lib/x64/Debug/pdfium.dll.lib"
    LIBS += "$$PWD/build/lib/debug/libqtpdfium.lib"

}

win32:release {
    LIBS += "$$PWD/pdfium/lib/x64/Release/pdfium.dll.lib"
    LIBS += "$$PWD/build/lib/release/libqtpdfium.lib"
}

macx:debug {
    LIBS += "$$PWD/build/lib/debug/libqtpdfium.a"
    LIBS += -framework AppKit -L"$$PWD/pdfium/lib/debug" -lpdfium -lfpdfapi -lfxge -lfpdfdoc -lfxcrt -lfx_agg -lfxcodec -lfx_lpng -lfx_libopenjpeg -lfx_lcms2 -lfx_freetype -ljpeg -lfdrm -lpwl -lbigint -lformfiller
}

macx:release {
    LIBS += "$$PWD/build/lib/release/libqtpdfium.a"
    LIBS += -framework AppKit -L"$$PWD/pdfium/lib/release" -lpdfium -lfpdfapi -lfxge -lfpdfdoc -lfxcrt -lfx_agg -lfxcodec -lfx_lpng -lfx_libopenjpeg -lfx_lcms2 -lfx_freetype -ljpeg -lfdrm -lpwl -lbigint -lformfiller
}
