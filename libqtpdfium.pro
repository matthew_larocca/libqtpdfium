#-------------------------------------------------
#
# Project created by QtCreator 2018-03-09T21:16:32
#
#-------------------------------------------------

QT       += core gui widgets

win32 {
    TARGET = libqtpdfium
}

unix {
    TARGET = qtpdfium
}

TEMPLATE = lib
CONFIG += staticlib
DEFINES += BUILD_LIBRARY

INCLUDEPATH += include pdfium/include

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/pdfdocumentview.cpp \
    src/pdfdocument.cpp \
    src/pdflibrary.cpp

HEADERS += \
    include/qtpdfium.h \
    include/pdfdocumentview.h \
    include/pdfdocument.h \
    include/pdflibrary.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

CONFIG(debug, debug|release){
    DESTDIR = "$$PWD/build/lib/debug"
}

CONFIG(release, debug|release) {
    DESTDIR = "$$PWD/build/lib/release"
}

FORMS += \
    src/pdfdocumentview.ui

